/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp-now-one-to-many-esp32-esp8266/

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#include <ArduinoJson.h>
#include <esp_now.h>
#include <WiFi.h>

#define BUTTON_MAIN 33
#define BUTTON_MEASURING 34
#define LED_PIN 18

static byte toggle_sw_memmory_measuring = 0;
bool DEVICE_ON = false;
bool MEASURING = false;
bool onlyonce = true;
int oxi;
int heart;
unsigned long previousMillis = 0;
unsigned long previousMillis2 = 0;
int ledState = LOW;

// MAC adress of main backpack controller
uint8_t broadcastAddress[] = {0x0C, 0xB8, 0x15, 0xC3, 0x3D, 0x74};

String jsondataOxi;
StaticJsonDocument<200> docdevice;

// function for receiving ESP-NOW data
void OnDataRecv(const uint8_t *mac, const uint8_t *incomingData, int len) {
  char* buff = (char*) incomingData; // char buffer
  String jsondataIn = String(buff); // converting into STRING
  Serial.print("Recieved ");
  Serial.println(jsondataIn); // JSON data will be printed

  DynamicJsonDocument dataIn(256);

  DeserializationError error = deserializeJson(dataIn, jsondataIn);

  if (!error) {
    if (dataIn["action"]["name"] == "measuring") {
      MEASURING = dataIn["action"]["value"];
    } else if (dataIn["action"]["name"] == "device_on") {
      DEVICE_ON = dataIn["action"]["value"];
    }

  } else {
    Serial.print(F("deserializeJson() failed: ")); // just in case of an ERROR of ArduinoJSON
    Serial.println(error.f_str());
    return;
  }

}

// function for sending ESP-NOW data
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void print_wakeup_reason() {
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch (wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 :
      if (DEVICE_ON == false) {
        DEVICE_ON = true;
      };
      break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n", wakeup_reason); break;
  }
}

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);

  pinMode(BUTTON_MAIN, INPUT);
  pinMode(BUTTON_MEASURING, INPUT);
  pinMode(LED_PIN, OUTPUT);

  // init ESP-NOW (2 way)
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // register peer
  esp_now_peer_info_t peerInfo;
  memset(&peerInfo, 0, sizeof(peerInfo));
  for (int ii = 0; ii < 6; ++ii )
  {
    peerInfo.peer_addr[ii] = (uint8_t) broadcastAddress[ii];
  }
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
    Serial.println("Failed to add peer");
    return;
  }

  // once ESP-NOW is successfully Init, we will register to send and receive data
  esp_now_register_recv_cb(OnDataRecv);
  esp_now_register_send_cb(OnDataSent);

  // print the wakeup reason
  print_wakeup_reason();

  // setup deep sleep
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, 1); // 1 = High, 0 = Low
}

void loop()
{
  unsigned long currentMillis = millis();

  // Check for keypress MAIN
  if (digitalRead(BUTTON_MAIN)) { // if pressed
    delay(30); // debounce

    if (digitalRead(BUTTON_MAIN)) { // if it is still pressed
      DEVICE_ON = false;
    }
    while (digitalRead(BUTTON_MAIN)); // wait until released
  }

  if (DEVICE_ON == true) {
    // Check for keypress MEASURING
    if (digitalRead(BUTTON_MEASURING)) { // if pressed
      delay(30); // debounce

      if (digitalRead(BUTTON_MEASURING)) { // if it is still pressed
        toggle_sw_memmory_measuring = !toggle_sw_memmory_measuring;

        if (toggle_sw_memmory_measuring)
          MEASURING = true;
        else
          MEASURING = false;
      }
      while (digitalRead(BUTTON_MEASURING)); // wait until released
    }
  }

  else if (DEVICE_ON == false) {
    digitalWrite(LED_PIN, LOW);
    MEASURING = false;

    if (onlyonce == true) {
      oxi = 0;
      heart = 0;

      jsondataOxi = ""; // clearing string after data is being sent

      docdevice["Device_Name"] = "Oximeter";
      docdevice["Mode"] = "idle"; // idle or measuring
      docdevice["Oxigen_Level"] = oxi;
      docdevice["Heart_Rate"] = heart;
      serializeJson(docdevice, jsondataOxi); // serilizing JSON

      esp_now_send(broadcastAddress, (uint8_t *) jsondataOxi.c_str(), sizeof(jsondataOxi) + 100); // sending "jsondataOxi"; adjust last number (length)
      Serial.println(jsondataOxi);

      onlyonce = false;
    }

    Serial.println("Let's go into deep sleep");
    esp_deep_sleep_start(); // go into deep sleep
  }

  if (MEASURING == true && DEVICE_ON == true) {
    if (currentMillis - previousMillis >= 100) { // 100 ms = blinking interval
      previousMillis = currentMillis; // save the last time you blinked the LED

      // if the LED is off turn it on and vice-versa:
      if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }

      // set the LED with the ledState of the variable:
      digitalWrite(LED_PIN, ledState);
    }

    if (currentMillis - previousMillis2 >= 500) { // 500 ms = sending interval
      previousMillis2 = currentMillis;

      oxi = random(70, 100);
      heart = random(40, 120);

      jsondataOxi = ""; // clearing string after data is being sent

      docdevice["Device_Name"] = "Oximeter";
      docdevice["Mode"] = "measuring"; // idle or measuring
      docdevice["Oxigen_Level"] = oxi;
      docdevice["Heart_Rate"] = heart;
      serializeJson(docdevice, jsondataOxi); // serilizing JSON

      esp_now_send(broadcastAddress, (uint8_t *) jsondataOxi.c_str(), sizeof(jsondataOxi) + 100); // sending "jsondataOxi"; adjust last number (length)
      Serial.println(jsondataOxi);
    }

    onlyonce = true;
  }

  else if (MEASURING == false && DEVICE_ON == true) {
    digitalWrite(LED_PIN, HIGH);

    if (onlyonce == true) {
      oxi = 0;
      heart = 0;

      jsondataOxi = ""; // clearing string after data is being sent

      docdevice["Device_Name"] = "Oximeter";
      docdevice["Mode"] = "idle"; // idle or measuring
      docdevice["Oxigen_Level"] = oxi;
      docdevice["Heart_Rate"] = heart;
      serializeJson(docdevice, jsondataOxi); // serilizing JSON

      esp_now_send(broadcastAddress, (uint8_t *) jsondataOxi.c_str(), sizeof(jsondataOxi) + 100); // sending "jsondataOxi"; adjust last number (length)
      Serial.println(jsondataOxi);

      onlyonce = false;
    }
  }
}
